# uboot-downloader
This is a python downloader program for extracting files for research purposes.  This program will extract chunks from a specified device using uboot commands and download it to your local PC over a tty connection.  Once all the chunks are downloaded, it concatenates them into a single binary.  The Depthcharge python package is used to facilitate the tty connection.

# Examples
Raw dump of /boot from spi nand

```$ python rkubootdumper.py rawdump --BASECHUNKNUM 0x800 --NUMCHUNKS 0x23ff```

Conversion of raw dump to a binary files

```$ python rkubootdumper.py parsedump --BASECHUNKNUM 0x800 --NUMCHUNKS 0x23ff```

Merge individual binary files into a single one

```$ python rkubootdumper.py mergebin --STARTCHUNKNUM 0x0 --NUMCHUNKS 0x23ff```
