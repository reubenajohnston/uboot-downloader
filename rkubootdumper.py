#!/usr/bin/env python3
import traceback
from depthcharge import Console, Depthcharge, log
from parse import parse
import argparse
import os
import sys

# example commands to dump /boot, /rootfs, /uboot from SPI NAND
# /boot dump
# $ python rkubootdumper.py rawdump --BASECHUNKNUM 0x800 --NUMCHUNKS 0x23ff --TARGET_RAM_ADDR 0x60100000
# $ python rkubootdumper.py parsedump --BASECHUNKNUM 0x800 --NUMCHUNKS 0x23ff
# $ python rkubootdumper.py mergebin --STARTCHUNKNUM 0 --NUMCHUNKS 0x23ff
# /rootfs dump
# $ python rkubootdumper.py rawdump --BASECHUNKNUM 0x2c00 --NUMCHUNKS 0xbfff
# $ python rkubootdumper.py parsedump --BASECHUNKNUM 0x2c00 --NUMCHUNKS 0xbfff
# $ python rkubootdumper.py mergebin --STARTCHUNKNUM 0 --NUMCHUNKS 0xbfff
# /idblock dump
# $ python rkubootdumper.py rawdump --BASECHUNKNUM 0x00000040 --NUMCHUNKS 0x37F
# $ python rkubootdumper.py parsedump --BASECHUNKNUM 0x00000040 --NUMCHUNKS 0x37F
# $ python rkubootdumper.py mergebin --STARTCHUNKNUM 0 --NUMCHUNKS 0x37F
# /uboot dump
# $ python rkubootdumper.py rawdump --BASECHUNKNUM 0x00000400 --NUMCHUNKS 0x3FF
# $ python rkubootdumper.py parsedump --BASECHUNKNUM 0x00000400 --NUMCHUNKS 0x3FF
# $ python rkubootdumper.py mergebin --STARTCHUNKNUM 0 --NUMCHUNKS 0x3FF
# /data dump
# $ python rkubootdumper.py rawdump --BASECHUNKNUM 0xEC00 --NUMCHUNKS 0x271DE
# example command to dump fdt from RAM
# $ python rkubootdumper.py ramchunkdump --BASEADDR 0x61f00000 --BASECHUNKNUM 0x0 --STARTCHUNKNUM 0x0 --SIZECHUNK 0x200 --NUMCHUNKS 0x47
# $ python rkubootdumper.py parsedump --STARTCHUNKNUM 0x0 --NUMCHUNKS 0x47
# $ python rkubootdumper.py mergebin --STARTCHUNKNUM 0x0 --NUMCHUNKS 0x47

def console_setup():
    console=Console("/dev/ttyUSB0",baudrate=115200)
    ctx = Depthcharge(console,arch="arm")
    return ctx

def printenv(ctx):
    print("printenv()")
    cmd_str = f"printenv"
    resp = ctx.send_command(cmd_str)
    print(resp)

def rksfc_info(ctx,device):
    print("printenv()")
    cmd_str = f"rksfc part {device:d}"
    resp = ctx.send_command(cmd_str)
    print(resp)
    
def rksfc_setup(ctx,device):
    cmd_str = f"rksfc device {device:d}"
    resp = ctx.send_command(cmd_str)
    print(resp)
    
def rksfc_read(ctx,dest_addr,blocknum,blockcount):
    cmd_str = f"rksfc read  0x{dest_addr:02x} 0x{blocknum:02x} 0x{blockcount:02x}"
    print(cmd_str)
    resp = ctx.send_command(cmd_str)
    return resp
    
def md(ctx,src_addr,size):
    cmd_str = f"md.l  0x{src_addr:02x} 0x{size:02x}"
    print(cmd_str)
    resp = ctx.send_command(cmd_str)
    return resp
    
def dump_block(ctx,dest_addr,blocknum,chunksize,filename):
    #read the block from spi nand into ram
    rksfc_read(ctx,dest_addr,blocknum,1)
    #tty read the block from ram
    resp=md(ctx,dest_addr,int(chunksize/4))#divide by 4 as we are using md.l (32-bits or 4 bytes at a time)
    #open the dump file
    fo=open(filename,'w+')
    #write the tty read block to dump file
    fo.write(resp)
    #close the dump file
    fo.close()
    
def dump_ramchunk(ctx,dest_addr,base_addr,chunknum,chunksize,filename):
    src_addr=base_addr+chunknum*chunksize
    #tty read the chunk from ram
    resp=md(ctx,src_addr,int(chunksize/4))#divide by 4 as we are using md.l (32-bits or 4 bytes at a time)
    #open the dump file
    fo=open(filename,'w+')
    #write the tty read chunk to dump file
    fo.write(resp)
    #close the dump file
    fo.close()
          
def parse_dump(binfilename,rawfilename):
    print("parse_dump("+binfilename+","+rawfilename)
    fo_rd=open(rawfilename,"r")
    fo_wr=open(binfilename,"wb+")
    #read in the rawfilename
    lines=fo_rd.readlines()
    #example tty's line out from md
    #61f000d0: 00006e65 03000000 51000000 3d000000    en.........Q...=
    mdstrparser="{address:x}: {word0:x} {word1:x} {word2:x} {word3:x}"
    #iterate over the lines in the rawfilename
    try:
        for line in lines:
            #parse the line into a 4-byte number (rockchip is big endian)
            r=parse(mdstrparser,line[0:-21]).named#ignore trailing whitespace and 16 bytes of ASCII
            ba=r['word0'].to_bytes(4,"little")
            #write bytes from 4-byte number to binfilename
            fo_wr.write(ba)
            ba=r['word1'].to_bytes(4,"little")
            #write bytes from 4-byte number to binfilename
            fo_wr.write(ba)
            ba=r['word2'].to_bytes(4,"little")
            #write bytes from 4-byte number to binfilename
            fo_wr.write(ba)
            ba=r['word3'].to_bytes(4,"little")
            #write bytes from 4-byte number to binfilename
            fo_wr.write(ba)                
        #close rawfilename
        fo_rd.close()
        #close binfilename
        fo_wr.close()
    except Exception as error:
        print(Exception)
        
    finally:
        pass

def appendbin(mergefilename,binfilename):
    os.system("cat {binfile} >> {mergebin}".format(binfile=binfilename,mergebin=mergefilename))

def usage():
    print("Usage : %s <COMMAND> --rawfilename <RAWFILENAME> --binfoldername <BINFOLDERNAME> --binfilename <BINFILENAME> --STARTCHUNKNUM <STARTCHUNKNUM> --NUMCHUNKS <NUMCHUNKS> --TARGET_RAM_ADDR <TARGET_RAM_ADDR> --SIZECHUNK <SIZECHUNK> --DEVICE <DEVICE>" % sys.argv[0])
    sys.exit(0)

def auto_int(x):
    return int(x, 0)    

def main(argc, argv):  
    if argc<2:
        usage()
    parser = argparse.ArgumentParser()
    parser.add_argument("command", help="printenv,rksfcinfo,rawdump,parsedump,mergebin,ramchunkdump",
                        type=str)
    parser.add_argument("--binfoldername", help="directory for binaries",
                        type=str,default="dump")
    parser.add_argument("--BASECHUNKNUM", help="int base block in SPI NAND or chunk in RAM",
                        type=auto_int, default=int(0x00000800))
    parser.add_argument("--STARTCHUNKNUM", help="int start block in SPI NAND or chunk in RAM",
                        type=auto_int, default=int(0x00000000))
    parser.add_argument("--NUMCHUNKS", help="int number of blocks to dump from SPI NAND or chunks from RAM",
                        type=auto_int, default=int(0x000023ff))
    parser.add_argument("--SIZECHUNK", help="number of bytes in SPI NAND block or chunks from RAM",
                        type=auto_int, default=int(512))
    parser.add_argument("--BASEADDR", help="base address",
                        type=auto_int, default=int(0x60000000))
    parser.add_argument("--TARGET_RAM_ADDR", help="buffer in RAM to use as scratchpad",
                        type=auto_int, default=int(0x60100000))
    parser.add_argument("--DEVICE", help="SPI NAND device number",
                        type=auto_int, default=int(0))                            

    args = parser.parse_args()        
    ctx=None

    try:       
        rawfilename_parsestr="{folder}/rawblk{{blknum:n}}.bin".format(folder=args.binfoldername)
        binfilename_parsestr="{folder}/binblk{{blknum:n}}.bin".format(folder=args.binfoldername)
        mergefilename="{folder}/mergedbins.bin".format(folder=args.binfoldername)

        #setup depthcharge if needed
        if args.command=="printenv" or args.command=="rksfcinfo" or args.command=="rawdump" or args.command=="ramchunkdump":
            log.info("Setup rkchip uboot tty session...")
            ctx = console_setup()

        if args.command=="rawdump" or args.command=="rksfcinfo":
            rksfc_setup(ctx,args.DEVICE)

        #do the tasks for specified command            
        if args.command=="printenv":
            printenv(ctx)
        #bdinfo
        #android_print_hdr       
        #fdtdump     
        elif args.command=="rksfcinfo":
            rksfc_info(ctx,args.DEVICE)
        elif args.command=="rawdump":
            log.info("Dump SPI NAND blocks from boot.img")
            for i in range(args.STARTCHUNKNUM,args.STARTCHUNKNUM+args.NUMCHUNKS+1):
                dump_block(ctx,args.TARGET_RAM_ADDR,args.BASECHUNKNUM+i,args.SIZECHUNK,rawfilename_parsestr.format(blknum=i))   
            log.info("Dump SPI NAND blocks finished")
        elif args.command=="parsedump":
            log.info("Convert rawdump to binary files...")
            for i in range(args.STARTCHUNKNUM,args.STARTCHUNKNUM+args.NUMCHUNKS+1):
                parse_dump(binfilename_parsestr.format(blknum=i),rawfilename_parsestr.format(blknum=i))
            log.info("Conversion finished")
        elif args.command=="mergebin":
            #create the empty mergebin
            os.system("touch {mergebin}".format(mergebin=mergefilename))
            for i in range(args.STARTCHUNKNUM,args.NUMCHUNKS+1):
                srcfilename=binfilename_parsestr.format(blknum=i)
                print("Merging "+srcfilename+" into "+mergefilename)                
                appendbin(mergefilename,srcfilename)
        elif args.command=="ramchunkdump":
            log.info("Dump ram chunks")           
            for i in range(args.STARTCHUNKNUM,args.STARTCHUNKNUM+args.NUMCHUNKS+1):
                dump_ramchunk(ctx,args.TARGET_RAM_ADDR,args.BASEADDR,args.BASECHUNKNUM+i,args.SIZECHUNK,rawfilename_parsestr.format(blknum=i))   
            log.info("Dump ram chunks finished")

    except Exception as error:
        print('Exception occurred')
        log.error(str(error))

        # Shown if DEPTHCHARGE_LOG_LEVEL=debug in environment
        log.debug(traceback.format_exc())

    finally:
        print("Complete")

if __name__ == "__main__":
    main(len(sys.argv), sys.argv)
 